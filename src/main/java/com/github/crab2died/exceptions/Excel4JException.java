package com.github.crab2died.exceptions;

/**
 * Created by lafangyuan on 2019/11/21.
 */
public class Excel4JException extends Exception {

    public Excel4JException() {
    }

    public Excel4JException(String message) {
        super(message);
    }

    public Excel4JException(String message, Throwable cause) {
        super(message, cause);
    }

    public Excel4JException(Throwable cause) {
        super(cause);
    }
}