package com.github.crab2died.sheet.wrapper;

import java.util.List;
import java.util.Set;

/**
 * Created by lafangyuan on 2019/11/21.
 */
public class NoTemplateSheetWrapper {
    /**
     * 待导出行数据
     */
    private List<?> data;

    /**
     * 基于注解的class
     */
    private List<String> headers;

    private Class clazz;

    private Set<String> configureColumn;

    /**
     * sheet名
     */
    private String sheetName;

    public NoTemplateSheetWrapper() {
    }

    public NoTemplateSheetWrapper(List<?> data, Class clazz, Set<String> configureColumn, String sheetName) {
        this.data = data;
        this.clazz = clazz;
        this.configureColumn = configureColumn;
        this.sheetName = sheetName;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public void setHeaders(List<String> headers) {
        this.headers = headers;
    }


    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Set<String> getConfigureColumn() {
        return configureColumn;
    }

    public void setConfigureColumn(Set<String> configureColumn) {
        this.configureColumn = configureColumn;
    }
}
